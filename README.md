# Host Machine Requirements
1. [Git](https://git-scm.com/)
1. [Vagrant](https://www.vagrantup.com/downloads.html)
2. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

# Getting Started

1. `git clone git@bitbucket.org:cbm3/automation-vm.git automation.test`
2. `cd automation.test`
3. `vagrant up`
4. `vagrant ssh`
5. `cd /var/www/html/automation.test/web`
6. `composer install`
7. `cp .env.example .env`
8. Add the following line to the end of your host file: 

    `192.168.33.10 automation.test db.automation.test wp.automation.test mx.automation.test`
    
# Available Domains

1. http://automation.test - Here you can access the base laravel application
2. http://db.automation.test - Here you can access PhpMyAdmin, which manages all databases for this VM.
3. http://wp.automation.test - Here you can access whatever code you choose to put inside automation.test/wordpress
4. http://mx.automation.test - Here you can access whatever code you choose to put in automation.test/modx

# PhpMyAdmin

1. **Username**: root
2. **Password**: password

# Migrating ModX pages to WordPress pages

The migration process works by first exporting your ModX pages into a wordpress friendly .csv file.
You can then use a number of plugins to import the csv file into WordPress. 
The plugins that we highly recommend are [WP All Import](http://www.wpallimport.com/) & [WP All Import SEO Plugin](http://www.wpallimport.com/documentation/plugins-themes/yoast-wordpress-seo/)

## Import your ModX database into the VM

Once you've followed the instructions under the **Getting Started** section you can proceed with the following instructions.
The first thing you need to do is export your production ModX database into an .sql file.
After you've saved the export file in a safe location, navigate to [db.automation.test](http://db.automation.test).
Using PhpMyAdmin, create a new database and user with full permissions over that database. Make sure to write down the database name, username and password somewhere safe.
Once, you've done that, import your ModX sql file into the new database.

You will need to set the credentials of your newly created database and user inside `automation.test/web/.env`.
Here are the values you will need to update: `DB_DATABASE_MX`, `DB_USERNAME_MX` and `DB_PASSWORD_MX`
 
## Export your ModX pages

Once your ModX database is installed in the VM, you can execute the terminal command that will export your pages.

Please note you will need to SSH into the VM in order to have access to the command.
You can SSH into the VM by executing `vagrant ssh` while inside the automation.test project directory.
Then, you'll need to navigate to the project's directory inside the VM: `cd /var/www/html/automation.test/web`.
Now you have access to the MoveModx command. The most basic usage looks like this: `php artisan MoveModx https://www.example.com` where https://www.example.com is the current location of the ModX app.

If your WordPress application is going to be hosted on a different domain than the ModX app, you'll need an additional flag: `--destination-url=https://www.example2.com`.

If you want the script to export images from page content you'll also need to provide an additional flag:
`--image-url=https://www.example2.com/images/content-images/` where the URL is the final location of where you will upload the exported images.

There is one more flag available with the MoveModx command: `--origin-base-value`. This flag is used when the ModX application has a `<base>` HTML element present in the template.
The `<base>` element will alter how relative URLs are interpreted by the browser. In order for MoveModx to download and export content images, and replace static content links it needs to successfully interpret the fully-qualified values of URLs.
If your ModX template does contain a `<base>` tag please provide its src value to the `--origin-base-value` flag. This will ensure the correct interpretation of relative URLs in the ModX content.

This is an example of the MoveModx command using all of the available flags: 
`php artisan moveModx https://www.modx-location.com --destination-url=https://www.wordpress-location.com --image-url=https://www.wordpress-location.com/wp-content/themes/mytheme/images/content/ --origin-base-value=https://www.modx-location.com`

Your CSV file will be exported into: automation.test/web/database/moveModx/pages_export.csv

Your .htaccess redirects will be exported into: automation.test/web/database/moveModx/htaccess_export.txt

Your content images will be exported into: automation.test/web/database/moveModx/images/

## Import the pages
Install the [WP All Import](http://www.wpallimport.com/) plugin, and [WP All Import SEO Plugin](http://www.wpallimport.com/documentation/plugins-themes/yoast-wordpress-seo/) into your WordPress application.
Please refer to the [documentation](http://www.wpallimport.com/documentation/) if you need assistance in importing the CSV file.

Keep in mind, that MoveModx will sometimes need to update page URI syntax in order to comply with WordPress standards.
When this is done, it's a good idea to setup redirects from the old URLs to the new ones. Doing this is easy.
Simply copy the contents from `automation.test/web/database/moveModx/htaccess_export.txt` into the .htaccess config file for your website.
For now, only Apache redirects are supported but in the future we plan to implement support for Nginx redirects as well.

Also don't forget to copy the files in `automation.test/web/database/moveModx/images/` into whatever directory is associated with the url you set with the `--image-url` flag.