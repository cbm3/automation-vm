<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Sunra\PhpSimple\HtmlDomParser;
use App\Console\Commands\MoveModxUtility;

class moveModx extends Command
{
    use MoveModxUtility\absoluteUrl;

    /**
     * The name and signature of the console command
     *
     * @var string
     */
    protected $signature = 'moveModx 
                            {origin-url : Provide the full base URL for the original domain. e.g https://www.origin.com}
                            {--destination-url= : Provide the full base URL for where the WordPress site will be hosted.}
                            {--image-url= : Provide the full base URL for where the exported images will be uploaded.}
                            {--origin-base-value= : If the ModX template uses a \<base\> tag, provide it\'s attribute\'s value.}';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Export ModX pages and blogs. Generate redirects for invalid URIs and ModX "Page Links".';

    /**
     * ModX Database Connection Name
     *
     * @var string
     */
    protected $mxConnectionName = 'modx';

    /**
     * ModX Database Connection
     *
     * @var \Illuminate\Database\Connection
     */
    protected $mxConnection = null;

    /**
     * WordPress Database Connection Name
     *
     * @var string
     */
    protected $wpConnectionName = 'wordpress';

    /**
     * WordPress Database Connection
     *
     * @var \Illuminate\Database\Connection
     */
    protected $wpConnection = null;

    /**
     * ModX pages that will be moved to WordPress
     *
     * @var array
     */
    protected $pages = [];

    /**
     * The number of pages pulled from ModX
     *
     * @var int
     */
    protected $pageCount = [];

    /**
     * Stores URIs that will need to be redirected
     *
     * @var array
     */
    protected $redirects = [];

    /**
     * Allow unpublished parent pages
     *
     * @var bool
     */
    protected $unpublishedParents = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->mxConnection = DB::connection($this->mxConnectionName);
        $this->wpConnection = DB::connection($this->wpConnectionName);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->getPages();
        if($this->confirmPageCount()) {
            $this->replaceImages();
            $this->replaceInvalidURIs();
            $this->collectWebLinks();
            $this->handleRedirects();
            $this->storeParentPageURIs();
            $this->replaceStaticLinks();
            $this->replaceModxLinks();
            $this->replaceModxTags();
            $this->cleanPageContent();
            $this->prepareExport();
            $this->exportPages();
            $this->exportRedirects();
            $this->printPagesMsg();
            $this->printRedirectsMsg();
        } else {
            $this->error('ERROR: Canceling export because not all pages were able to be retrieved.');
            $this->warn('Please try debugging this script or performing the export manually.');
            exit;
        }
    }

    /**
     * A method to customize data before export on a per-site basis.
     * Feel free to gut this method and start from scratch if you're migrating a new site.
     */
    private function handleData()
    {

    }

    /**
     * A method to customize/spell-check redirects before links are replaced and before export occurs.
     * Feel free to gut this method and start from scratch if you're migrating a new site.
     */
    private function handleRedirects()
    {
        function spellCheck($string)
        {
//            $string = str_replace('law-enforcement-officer(-leo)','law-enforcement-officer', $string);
            return $string;
        }
        foreach($this->redirects as $from => $to) {
            $this->redirects[$from] = spellCheck($to);
        }
        foreach($this->pages as $page) {
            $page->generated_uri_string = spellCheck($page->generated_uri_string);
            $page->uri = spellCheck($page->uri);
            $page->alias = spellcheck($page->alias);
            if(isset($page->parent_page_uri)) {
                $page->parent_page_uri = spellCheck($page->parent_page_uri);
            }
        }
    }

    /**
     * Download images and replace their URLs in the content
     * @return void
     */
    private function replaceImages()
    {
        $imgUrl = rtrim($this->option('image-url'), '/').'/';
        $originUrl = rtrim($this->argument('origin-url'), '/').'/';
        $baseValue = rtrim($this->option('origin-base-value'), '/') . '/';

        if($imgUrl !== '/') {

            foreach ($this->pages as $page) {

                if(empty($page->content)) {
                    continue;
                }

                $dom = HtmlDomParser::str_get_html($page->content);
                $images = $dom->find('img');

                if(empty($images)) {
                   continue;
                }

                foreach ($images as $image) {

                    //Encode image source if it contains white space
                    if (preg_match('/\s/', $image->src) === 1) {
                        $image->src = rawurlencode($image->src);
                    }

                    $relativeSrc = strpos($image->src,'http') === false &&
                                   substr($image->src,0,2) !== '//';

                    //Determine the full URL of the hosted image
                    if($relativeSrc && $baseValue == '/') {
                        $hostedImage = $this->urlToAbsolute($originUrl.$page->uri,$image->src);
                    } elseif ($relativeSrc && $baseValue != '/') {
                        $hostedImage = $this->urlToAbsolute($baseValue,$image->src);
                    } else {
                        $hostedImage = $image->src;
                    }

                    //Determine new image name, path and extension
                    $imageExt = pathinfo($hostedImage, PATHINFO_EXTENSION);
                    $newFile = md5($hostedImage).'.'.$imageExt;
                    $newFilePath = database_path('moveModx/images/'.$newFile);

                    if (file_exists($newFilePath)) {
                        //Replace image source with new image source
                        $image->src = $imgUrl.$newFile;
                    } else {
                        //Collect remote document meme type
                        $ch = curl_init($hostedImage);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_exec($ch);
                        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                        curl_close($ch);
                        //Download remote image if it has a valid meme type
                        if(strpos($contentType, 'image') !== false) {
                            $this->info('Downloading: ' . $hostedImage);
                            $ch = curl_init($hostedImage);
                            $fp = fopen($newFilePath, 'wb');
                            curl_setopt($ch, CURLOPT_FILE, $fp);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_exec($ch);
                            curl_close($ch);
                            fclose($fp);
                            $image->src = $imgUrl.$newFile;
                        } else {
                            $this->warn('Failed to download resource: ' . $image->src);
                        }
                    }
                }
                //After image sources are replaced, save page HTML
                $page->content = strval($dom);
            }
        }
    }

    /**
     * Execute additional query to check that the pages retrieved from the DB
     * matches the number of pages in the database
     *
     * NOTE: This method is a fail-safe. It confirms nothing went wrong during export.
     *
     * @return bool
     */
    private function confirmPageCount()
    {
        return $this->pageCount === $this->countModXPages();
    }

    /**
     * Replace page URIs with ones that adhere to WordPress standards.
     * Also fill $redirects with invalid URIs.
     *
     * @return void
     */
    private function replaceInvalidURIs()
    {
        //Generate our own page URIs by traversing the parents of each page and collecting aliases
        $this->sortPagesByModxId();
        foreach($this->pages as $page) {
            $page->generated_uri = $this->appendParentPageURI($page);
            $page->generated_uri = array_reverse($page->generated_uri);
            $page->generated_uri[] = $page->alias;
        }
        //Sanitize URIs that we generated
        foreach($this->pages as $page) {
            $this->sanitizePageURI($page);
        }
        //Compare our URIs to the static ones stored in ModX. Redirects will be setup for ones that don't match.
        foreach($this->pages as $page) {

            $page->uri = rtrim($page->uri, '/');
            $originalURI  = explode('/', $page->uri);
            $cleanURI = rtrim($page->generated_uri_string, '/') . '/';
            $uriMatch = true;

            foreach($originalURI as $key => $uriBit) {
                if(!isset($page->generated_uri[$key]) || $page->generated_uri[$key] !== $uriBit) {
                    $uriMatch = false;
                }
            }

            foreach($page->generated_uri as $key => $uriBit) {
                if(!isset($originalURI[$key]) || $originalURI[$key] !== $uriBit) {
                    $uriMatch = false;
                }
            }

            if (!$uriMatch && $page->published === 'publish') {
                $this->redirects[$page->uri] = $cleanURI;
                $page->uri = $cleanURI;
            }

            if (!$uriMatch) {
                $page->uri = $cleanURI;
            }

        }
    }

    /**
     * Iterate through stored pages to add the parent_page_uri property
     * This property will be needed during import
     */
    private function storeParentPageURIs()
    {
        foreach($this->pages as $page) {
            $page->parent_page_uri = '';
            if ($page->parent_id != 0) {
                $page->parent_page_uri = $this->pages[$page->parent_id]->generated_uri_string;
            }
        }
    }

    /**
     * Replace local static links
     * @return void
     */
    private function replaceStaticLinks()
    {
        $originUrl = rtrim($this->argument('origin-url'), '/').'/';
        $destinationUrl = rtrim($this->option('destination-url'), '/') . '/';
        $baseValue = rtrim($this->option('origin-base-value'), '/') . '/';

        foreach ($this->pages as $page) {

            if (empty($page->content)) {
                continue;
            }

            $dom = HtmlDomParser::str_get_html($page->content);
            $links = $dom ? $dom->find('a') : [];

            foreach ($links as $link) {

                //If this is a ModX, anchor, mailto or tel link, then skip it.
                if (preg_match('/^(#.*|\[\[\~.*|mailto|tel)/i', $link->href) === 1 || empty($link->href)) {
                    continue;
                }

                $relativeSrc = strpos($link->href,'http') === false &&
                               substr($link->href,0,2) !== '//';

                //Determine the full URL of the links
                if($relativeSrc && $baseValue == '/') {
                    $link->href = $this->urlToAbsolute($originUrl.$page->uri,$link->href);
                } elseif ($relativeSrc && $baseValue != '/') {
                    $link->href = $this->urlToAbsolute($baseValue,$link->href);
                }

                //Replace the origin domain with the destination domain
                if ($destinationUrl != '/') {
                    $replace = rtrim($originUrl, '/');
                    $with = rtrim($destinationUrl, '/');
                    $link->href = str_replace($replace, $with, $link->href);
                }

                //Use the redirects to correct any invalid links
                foreach($this->redirects as $from => $to) {

                    $from = $destinationUrl == '/' ? $originUrl.$from : $destinationUrl.$from;
                    $to = $destinationUrl == '/' ? $originUrl.$to : $destinationUrl.$to;
                    $href = rtrim($link->href, '/');

                    if ($href == $from) {
                        $link->href = $to;
                    }
                }
            }

            $page->content = strval($dom);

        }
    }

    /**
     * Replace ModX link tags with full URLs
     * This will replace ModX links (e.g [[~1]]) that point to both pages and "Web Links"
     * @return void
     */
    private function replaceModxLinks()
    {
        $baseURL = rtrim($this->argument('origin-url'), '/');
        $baseURL = $this->option('destination-url') ?: $baseURL;

        $webLinks = [];
        foreach($this->getWebLinks() as $webLink) {
            $push = new \stdClass();
            $pageId = str_replace('[[~', '', $webLink->content);
            $pageId = str_replace(']]', '', $pageId);
            $push->id = $webLink->id;
            if (isset($this->pages[$pageId])) {
                $push->generated_uri_string = $this->pages[$pageId]->generated_uri_string;
                $webLinks[] = $push;
            }
        }

        $pagesAndWebLinks = array_merge($this->pages,$webLinks);
        foreach ($this->pages as $page) {
            foreach($pagesAndWebLinks as $link) {
                $linkUri = empty($link->generated_uri_string) ? '' : '/' . $link->generated_uri_string . '/';
                $page->content = str_replace('[[~'.$link->id.']]', $baseURL . $linkUri, $page->content);
            }
        }
    }

    /**
     * Replace custom ModX tags commonly used at Your Company
     *
     * @return void
     */
    private function replaceModxTags()
    {
        foreach ($this->pages as $page) {
            $page->content = preg_replace('/\[\[\$firm\]\]/i', '[firm]', $page->content);
            $page->content = preg_replace('/\[\[\$phone\]\]/i', '[phone]', $page->content);

            $page->longtitle = preg_replace('/\[\[\$firm\]\]/i', '[firm]', $page->longtitle);
            $page->longtitle = preg_replace('/\[\[\$phone\]\]/i', '[phone]', $page->longtitle);

            $page->description = preg_replace('/\[\[\$firm\]\]/i', '[firm]', $page->description);
            $page->description = preg_replace('/\[\[\$phone\]\]/i', '[phone]', $page->description);
        }
    }

    /**
     * Replace common content discrepancies found at Your Company
     *
     * @return void
     */
    private function cleanPageContent()
    {
        //Replace broken comment tags
        foreach ($this->pages as $page) {
            $page->content = str_replace('[[&lt;', '<', $page->content);
            $page->content = str_replace('&gt;]]', '>', $page->content);
        }
    }

    /**
     * Remove excess data from pages that won't be needed in the export file.
     * Sort pages array so that higher-level pages will be imported first.
     * Activate handleData hook, which will process data before export.
     *
     * @return void
     */
    private function prepareExport()
    {
        $sortByParentCount = function($a,$b) {
            return count($a->generated_uri) > count($b->generated_uri);
        };
        uasort($this->pages, $sortByParentCount);
        $this->handleData();
        foreach($this->pages as $page) {
            unset($page->generated_uri);
            unset($page->generated_uri_string);
            unset($page->parent_id);
        }
    }

    /**
     * Export pages to CSV File
     *
     * @return void
     */
    private function exportPages()
    {
        $columns = [
            'modx_id',
            'title',
            'content',
            'seo_title',
            'seo_description',
            'uri',
            'published',
            'alias',
            'parent_page_uri',
        ];

        $file = fopen(database_path('moveModx/pages_export.csv'), 'w');
        fputcsv($file, $columns);

        foreach($this->pages as $page) {
            fputcsv($file, (array) $page);
        }

        fclose($file);
    }

    /**
     * Save generated redirects to an export file
     *
     * @return void
     */
    private function exportRedirects()
    {
        $file = fopen(database_path('moveModx/htaccess_export.txt'), 'w');

        foreach($this->redirects as $from => $to) {
            $from = ltrim($from, '/');
            $from = rtrim($from, '/') . '/?';
            $to = ltrim($to, '/');
            $to = rtrim($to, '/') . '/';
            $redirect = "RewriteRule ^".$from."$ ".$to." [R=301,L]";
            fwrite($file, $redirect . PHP_EOL);
        }

        fclose($file);
    }

    /**
     * Print confirmation message for successful pages export
     *
     * @return void
     */
    private function printPagesMsg()
    {
        $this->info('SUCCESS: ' . $this->pageCount . ' pages have been exported to: ' . database_path('moveModx/pages_export.csv'));
    }

    /**
     * Print redirects message
     *
     * @return void
     */
    private function printRedirectsMsg()
    {
        $count = count($this->redirects);
        if ($count) {
            $this->info('SUCCESS: ' . $count . ' redirects have been saved to  ' . database_path('moveModx/htaccess_export.txt'));
        } else {
            $this->info('No redirects were created for this migration.');
        }
    }

    /**
     * Add ModX 'Web Links' to the redirects property
     *
     * @return void
     */
    private function collectWebLinks()
    {
        $webLinks = $this->getWebLinks();
        foreach($webLinks as $webLink) {
            $pageId = str_replace('[[~', '', $webLink->content);
            $pageId = str_replace(']]', '', $pageId);
            if (isset($this->pages[$pageId])) {
                $this->redirects[$webLink->uri] = $this->pages[$pageId]->uri;
            }
        }
    }

    /**
     * Remove invalid characters and invalid syntax from URI
     *
     * @return void
     */
    private function sanitizePageURI($page)
    {
        //If an alias contains forward slash, replace them with dashes
        foreach ($page->generated_uri as $key => $uriBit) {
            $uriBit = str_replace('/', '-', $uriBit);
            $page->generated_uri[$key] = $uriBit;
        }
        $generatedURI = implode('/', $page->generated_uri);
        //Convert CamelCase to snake-case
        $generatedURI = preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '-$0', $generatedURI);
        $generatedURI = strtolower($generatedURI);
        $generatedURI = trim($generatedURI, '-');
        $generatedURI = str_replace(',', '', $generatedURI);
        $generatedURI = str_replace('---', '-', $generatedURI);
        $generatedURI = str_replace('--', '-', $generatedURI);
        $generatedURI = str_replace('-/-', '/', $generatedURI);
        $generatedURI = str_replace('/-', '/', $generatedURI);
        $generatedURI = str_replace('-/', '/', $generatedURI);
        $generatedURI = str_replace('//', '/', $generatedURI);
        $generatedURI = str_replace('.html', '', $generatedURI);
        $generatedURI = str_replace('.htm', '', $generatedURI);
        $generatedURI = str_replace('.aspx', '', $generatedURI);
        $generatedURI = str_replace('.asp', '', $generatedURI);
        $page->generated_uri = explode('/', $generatedURI);
        $page->generated_uri_string = $generatedURI;
        $page->alias = end($page->generated_uri);
    }

    /**
     * Traverse a page's parents to construct its true URI.
     * This 'true URI' will be stored in the page's generated_uri property
     *
     * @return array
     */
    private function appendParentPageURI($page, $uri = [])
    {
        if($page->parent_id === 0) {
            return $uri;
        }

        //If the page's parent was deleted, then the path ends here
        if(!isset($this->pages[$page->parent_id])) {
            $page->published = 'draft';
            $page->parent_id = 0;
            return $uri;
        }

        $parent = $this->pages[$page->parent_id];

        //If we don't allow unpublished parent pages & this one is unpublished, then skip this page
        if (!$this->unpublishedParents && $parent->published !== 'publish') {
            if($parent->parent_id === 0) {
                return $uri;
            }
            $parent = $this->pages[$parent->parent_id];
        }

        $uri[] = $parent->alias;
        return $this->appendParentPageURI($parent, $uri);
    }

    /**
     * The array key for each page will be its ModX ID
     *
     * @return void
     */
    private function sortPagesByModxId()
    {
        $sorted = [];
        foreach($this->pages as $page) {
            $sorted[$page->id] = $page;
        }
        $this->pages = $sorted;
    }

    /**
     * Retrieve pages from the ModX database and store them in the $pages property
     *
     * @return void
     */
    private function getPages()
    {
        $query = " 
                SELECT
                  `ma`.`id`, `ma`.`pagetitle`, `ma`.`content`, `ma`.`longtitle`, `ma`.`description`, 
                  `ma`.`parent` as 'parent_id', `ma`.`uri`,
                  (CASE WHEN `ma`.`published` = 1 THEN 'publish' ELSE 'draft' END) as `published`,
                  (CASE WHEN `ma`.`alias` = 'index' THEN '' ELSE `ma`.`alias` END) as `alias`
                FROM `modx_site_content` `ma`
                WHERE 
                  `ma`.`deleted` = 0
                  AND `ma`.`alias` != '404error'
                  AND `ma`.`alias` != '404-error'
                  AND `ma`.`alias` != '404'
                  AND `ma`.`alias` != 'sitemap'
                  AND `ma`.`alias` != 'html-sitemap'
                  AND `ma`.`alias` != 'internal-sitemap'
                  AND `ma`.`alias` != 'internal-site-map'
                  AND `ma`.`alias` != 'site-map' 
                  AND `ma`.`alias` != 'search-results'
                  AND `ma`.`alias` != 'thank-you' 
                  AND `ma`.`alias` != 'form-error'
                  AND `ma`.`alias` != 'blog'
                  AND `ma`.`uri` NOT LIKE '%blog/%' 
                  AND `ma`.`alias` != 'upgrade-modx' 
                  AND `ma`.`class_key` != 'modWebLink'
                  AND `ma`.`class_key` != 'Article'
                  AND `ma`.`class_key` != 'ArticlesContainer'";
        $this->pages = $this->mxConnection->select($query);
        $this->pageCount = count($this->pages);
    }

    /**
     * Returns the number of pages in the ModX DB that should have been migrated
     *
     * @return int
     */
    private function countModXPages()
    {
        $query = "SELECT count(`ma`.`id`) as `page_count` 
                    FROM `modx_site_content` `ma`
                    WHERE `ma`.`deleted` = 0
                      AND `ma`.`alias` != '404error'
                      AND `ma`.`alias` != '404-error'
                      AND `ma`.`alias` != '404'
                      AND `ma`.`alias` != 'sitemap'
                      AND `ma`.`alias` != 'html-sitemap'
                      AND `ma`.`alias` != 'internal-sitemap'
                      AND `ma`.`alias` != 'internal-site-map'
                      AND `ma`.`alias` != 'site-map'
                      AND `ma`.`alias` != 'search-results'
                      AND `ma`.`alias` != 'thank-you'
                      AND `ma`.`alias` != 'form-error'
                      AND `ma`.`alias` != 'blog'
                      AND `ma`.`uri` NOT LIKE '%blog/%'
                      AND `ma`.`alias` != 'upgrade-modx'
                      AND `ma`.`alias` != 'upgrade-modx'
                      AND `ma`.`class_key` != 'modWebLink'
                      AND `ma`.`class_key` != 'Article'
                      AND `ma`.`class_key` != 'ArticlesContainer'";
        return $this->mxConnection->select($query)[0]->page_count;
    }

    /**
     * Retrieve all ModX 'Web Links' from the database
     *
     * @return array
     */
    private function getWebLinks()
    {
        $query = "
                SELECT
                  `ma`.`id`, `ma`.`pagetitle`, `ma`.`content`, `ma`.`uri`
                FROM `modx_site_content` `ma`
                LEFT JOIN `modx_site_content` `mb` ON `ma`.`parent` = `mb`.`id`
                WHERE
                  `ma`.`deleted` = 0
                  AND `ma`.`published` = 1
                  AND `ma`.`alias` != '404error'
                  AND `ma`.`alias` != '404-error'
                  AND `ma`.`alias` != '404'
                  AND `ma`.`alias` != 'sitemap'
                  AND `ma`.`alias` != 'html-sitemap'
                  AND `ma`.`alias` != 'internal-sitemap'
                  AND `ma`.`alias` != 'internal-site-map'
                  AND `ma`.`alias` != 'site-map'
                  AND `ma`.`alias` != 'search-results'
                  AND `ma`.`alias` != 'thank-you'
                  AND `ma`.`alias` != 'form-error'
                  AND `ma`.`alias` != 'blog'
                  AND `ma`.`uri` NOT LIKE '%blog/%'
                  AND `ma`.`alias` != 'upgrade-modx'
                  AND `ma`.`class_key` = 'modWebLink'
                  AND `ma`.`class_key` != 'Article'
                  AND `ma`.`class_key` != 'ArticlesContainer'";
        return $this->mxConnection->select($query);
    }
}
