<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;
use App\Jobs\MoveModx;
use Illuminate\View\View;

class Wizard extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * Upload an SQL file and store its unique name in the user's session
     * @TODO: If upload is attempted after modx-sql-upload value already exists, reject request.
     */
    public function uploadSql(Request $request)
    {
        $fileToSave = uniqid().'.sql';
        $errors = [];

        $validation = Validator::make($request->all(), [
            'file' => 'required|file|max:3145728'
        ]);

        if($validation->fails()) {
            $errors = $validation->errors();
        }

        if(isset($request->file) && !empty($request->file) &&
            pathinfo($request->file->getClientOriginalName(), PATHINFO_EXTENSION) !=='sql'){
            $errors['file'][] = 'Invalid file type.';
        }

        if (!empty($errors)) {
            return response()->json($errors, 422);
        }

        $request->file('file')->storeAs('modx-databases', $fileToSave);
        $request->session()->put('modx-sql-upload', $fileToSave);

        return response()->json(['success'=> true]);
    }

    /**
     * @param Request $request
     * @return Response
     * Validate and then store user input in session data
     * @TODO: If application information is already stored in session, reject request.
     */
    public function appInformation(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'origin-domain' => 'required|active_url',
            'destination-domain' => 'required_if:destination-domain-check,1|url',
            'destination-image-path' => 'required|url',
            'origin-base-value' => 'required_if:origin-base-value-check,1|url',
        ]);

        if($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $request->session()->put('origin-domain', $request->input('origin-domain'));
        $request->session()->put('destination-domain', $request->input('destination-domain', ''));
        $request->session()->put('destination-image-path', $request->input('destination-image-path', ''));
        $request->session()->put('origin-base-value', $request->input('origin-base-value', ''));

        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return Response
     * Dispatch a new MoveModx job to the queue
     * @TODO: Validating user session data before adding to queue
     * @TODO: If job has already been queued by this client, do not allow for another one to be queued
     */
    public function queueJob(Request $request)
    {
        MoveModx::dispatch([
            'origin-domain' => $request->session()->get('origin-domain'),
            'destination-domain' => $request->session()->get('destination-domain'),
            'destination-image-path' => $request->session()->get('destination-image-path'),
            'origin-base-value' => $request->session()->get('origin-base-value'),
            'modx-database' => $request->session()->get('modx-sql-upload'),
            'session-id' => $request->session()->getId(),
            'csrf-token' => csrf_token()
        ]);

        return response()->json(['success' => true, 'queueCount' => Queue::size()]);
    }

    /**
     * @param Request $request
     * @return Response
     * If there is an export file associated with the user, this method will prompt download
     */
    public function downloadExport(Request $request)
    {
        $filePath = storage_path('app/modx-exports/'.csrf_token().'.zip');
        if(file_exists($filePath)) {
            return response()->download($filePath);
        } else {
            return abort(404);
        }
    }
}
