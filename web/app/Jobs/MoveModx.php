<?php

namespace App\Jobs;

use App\Events;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Chumper\Zipper\Zipper;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class MoveModx
 * @package App\Jobs
 */
class MoveModx implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The time-out value for this job (in seconds)
     *
     * @var int|null
     */
    public $timeout = 600;

    /**
     * The number of times the job may be attempted
     *
     * @var int|null
     */
    public $tries = 1;

    /**
     * Stored user session data
     *
     * @var array
     */
    public $userSession = [];

    /**
     * Create a new job instance.
     *
     * @param array $userSession
     * @return void
     */
    public function __construct($userSession)
    {
        $this->userSession = $userSession;
//        $this->timeout = env('QUEUE_TIMEOUT', 120);
    }

    /**
     * Handle a job failure
     *
     * @param  \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        event(new Events\MoveModxFailed($this->userSession));
        event(new Events\MoveModxCompleted($this->userSession));
    }

    /**
     * Execute the job
     *
     * @param array
     * @return void
     */
    public function handle()
    {
        $this->cleanDatbase();
        $this->importModx();
        $this->export();
        $this->zipExports();
        $this->cleanUp();
        event(new Events\MoveModxCompleted($this->userSession));
        event(new Events\MoveModxSucceeded($this->userSession));
    }

    /**
     * Clear out the ModX database
     *
     * @return void
     */
    protected function cleanDatbase()
    {
        exec("mysql -u ".env('DB_USERNAME')." -p".env('DB_PASSWORD')." -e 'DROP DATABASE ".env('DB_DATABASE_MX')."'");
        exec("mysql -u ".env('DB_USERNAME')." -p".env('DB_PASSWORD')." -e 'CREATE DATABASE ".env('DB_DATABASE_MX')."'");
        exec("mysql -u ".env('DB_USERNAME')." -p".env('DB_PASSWORD')." -e \"GRANT DROP, CREATE ON ".env('DB_DATABASE_MX').".* TO '".env('DB_USERNAME_MX')."'@'localhost' IDENTIFIED BY '".env('DB_PASSWORD_MX')."';\"");
    }

    /**
     * Import the user's ModX database
     *
     * @TODO: Allow only the needed permissions for modxStorage mysql user
     * @return void
     */
    protected function importModx()
    {
        $file = $this->userSession['modx-database'];
        exec("mysql -u ".env('DB_USERNAME_MX')." -p".env('DB_PASSWORD_MX')." ".env('DB_DATABASE_MX')." < ".storage_path('app/modx-databases/'.$file));
    }

    /**
     * Execute the MoveModx command
     *
     * @return void
     */
    protected function export()
    {
        $arguments = [];
        if(!empty($this->userSession['origin-domain'])) {
            $arguments['origin-url'] = $this->userSession['origin-domain'];
        }
        if(!empty($this->userSession['destination-domain'])) {
            $arguments['--destination-url'] = $this->userSession['destination-domain'];
        }
        //For development and testing we will not attempt to download images
        if (env('APP_ENV', 'local') == 'production') {
            if(!empty($this->userSession['destination-image-path'])) {
                $arguments['--image-url'] = $this->userSession['destination-image-path'];
            }
        }
        if(!empty($this->userSession['origin-base-value'])) {
            $arguments['--origin-base-value'] = $this->userSession['origin-base-value'];
        }
        Artisan::call('moveModx', $arguments);
    }

    /**
     * Zip the exported files and store them using session ID
     *
     * @TODO: Create a cronjob that will delete zipped files that are older than an hour
     * @return void
     */
    public function zipExports()
    {
        try {
            $zipper = new Zipper();
            $zipper->make('storage/app/modx-exports/'.$this->userSession['csrf-token'].'.zip')
                ->add('database/moveModx')
                ->close();
        } catch (Exception $e) {
            $this->failed($e);
        }
    }

    /**
     * Clean up non-compressed files
     *
     * @TODO: Move the database/moveModx directory to storage/moveModx
     * @return void
     */
    public function cleanUp()
    {
        $gitignore =
            '# Ignore everything in this directory'.PHP_EOL.
            '*'.PHP_EOL.
            '# Except this file'.PHP_EOL.
            '!.gitignore'.PHP_EOL.
            '!images';

        File::deleteDirectory(database_path('moveModx'));
        File::makeDirectory(database_path('moveModx'), 0775);
        File::makeDirectory(database_path('moveModx/images'), 0775);
        File::put(database_path('moveModx/.gitignore'), $gitignore);
        File::put(database_path('moveModx/images/.gitignore'), $gitignore);
    }

}
