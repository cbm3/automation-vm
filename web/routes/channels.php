<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

//Broadcast::channel('App.User.{id}', function ($user, $id) {
//    return (int) $user->id === (int) $id;
//});

Broadcast::channel('wizard-event-queue', function() {
    return true;
});

Broadcast::channel('wizard-event-private.{csrfToken}', function ($user, $csrfToken) {
    return $csrfToken == csrf_token();
});
