<?php
use App\Events\MoveModxCompleted;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/upload-modx', 'Wizard@uploadSql');
Route::post('/app-information', 'Wizard@appInformation');
Route::post('/queue-job', 'Wizard@queueJob');
Route::get('/download-export', 'Wizard@downloadExport');
